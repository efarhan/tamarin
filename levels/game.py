'''
Created on 23 sept. 2013

@author: efarhan
'''
import pygame
import random
import math
from engine.scene import Scene
from game_object.player import Player
from game_object.girl import Girl
from game_object.text import Text
from engine.init import get_screen_size
from engine.event import get_up, get_down, get_right, get_left, get_retry
class Game(Scene):
    def init(self):
        self.factor = 5
        self.screen_pos = (0,0)
        self.player = Player(pos=(-get_screen_size()[0]/2+5*32/2,-200),factor=self.factor)
        self.player.lock = ((0,int(self.player.pos[0]+get_screen_size()[0]*2)))
        self.girls = [Girl((200+get_screen_size()[0]/2,-200),factor=self.factor)]
        self.texts = [Text((get_screen_size()[0]/2,0),'single life')]
        pygame.mixer.music.load('data/music/loop1.ogg')
        pygame.mixer.music.play()
        self.tried = False
        self.down_tried = False
        self.heart_factor = 1
        self.background = pygame.Surface(get_screen_size())
        self.background = self.background.convert()
        self.background.fill(pygame.Color(255,255,255))
        self.end = -1
        self.end_reached = False
        self.heart_counter = 0
        
    def fill_text(self):
        step = ['dating', 'romance','passion', 'love', 'engagement','married life']
        self.texts = [Text((get_screen_size()[0]*step.index(text)+get_screen_size()[0]+self.player.pos[0],0), text) for text in step]
        self.end = self.texts[len(self.texts)-1].pos[0]-get_screen_size()[0]/2
    def loop(self, screen):
        if(not pygame.mixer.music.get_busy()):
            pygame.mixer.music.play()
        screen.blit(self.background,(0,0))
        y_line = get_screen_size()[1]/2-self.player.pos[1]+self.player.size[1]/2
        pygame.draw.line(screen,pygame.Color(0,0,0),(0,y_line),(get_screen_size()[0],y_line),self.factor)
        up, down = get_up(),get_down()
        
        for g in self.girls:
            g.loop(screen, self.screen_pos)
            if(up and not self.tried and not g.follow and math.fabs(self.player.pos[0]-g.pos[0])<100):
                if(g.try_relation()):
                    self.player.show_heart = True
                    for g2 in self.girls:
                        if(g2!= g and g2.follow):
                            g2.breakup()
                            break
                    del self.texts[:]
                    self.fill_text()
                else:
                    for g2 in self.girls:
                        if g2.follow:
                            g2.breakup()
                            break
                self.tried = True
                
                break
            if(not up):
                self.tried = False
            if(down and not self.down_tried and not self.end_reached and g.follow):
                del self.texts[:]
                g.breakup()
        
        for t in self.texts:
            t.loop(screen,self.screen_pos)
        if(down and not self.down_tried and not self.end_reached):
            
            self.down_tried = True
        else:
            self.down_tried = False
            
            
        #TODO add girls no matter what, depending on position
        if not self.player.show_heart:
            after_girl = False
            g_pos = -1
            for g in self.girls:
                if (self.player.pos[0]+get_screen_size()[0]/2<g.pos[0]<self.player.pos[0]+get_screen_size()[0]*3/2):
                    after_girl = True
                elif(self.player.pos[0]-get_screen_size()[0]/2<g.pos[0]<self.player.pos[0]+get_screen_size()[0]/2):
                    g_pos = g.pos[0]
            if not after_girl and g_pos != -1:
                self.girls.append(Girl((g_pos+get_screen_size()[0],-200), factor=self.factor))
                self.player.lock = ((0,int(self.player.pos[0]+get_screen_size()[0]*2)))
        #if tried but fail check if there is a girl after if not create one
        '''if((self.tried and not self.player.show_heart) or (self.down_tried and self.player.show_heart)):
            self.player.show_heart = False
            self.player.state = 0
            #check if girl after
            self.end = -1
            
            after_girl = False
            for g in self.girls:
                if (self.player.pos[0]+get_screen_size()[0]/2<g.pos[0]<self.player.pos[0]+get_screen_size()[0]*3/2):
                    after_girl = True
            if not after_girl :
                self.girls.append(Girl((self.player.pos[0]+get_screen_size()[0],-200), factor=self.factor))
            self.player.lock = ((0,int(self.player.pos[0]+get_screen_size()[0]*3)))'''
        if(self.tried and self.player.show_heart):        
            after_girl = False
            after_girls = []
            for g in self.girls:
                if (self.player.pos[0]+get_screen_size()[0]/2<g.pos[0]):
                    after_girl = True
                    after_girls.append(g)
            if after_girl:
                for g in after_girls:
                    self.girls.remove(g)
            self.player.lock = ((0,self.player.pos[0]+get_screen_size()[0]*(len(self.texts)+1)))
            
        #girl break up
        if(self.player.show_heart and not self.end_reached):
            try_breakup = False
            for i in range(len(self.texts)-1):
                if(self.player.state == i and self.texts[i].pos[0]-get_screen_size()[0]/2<self.player.pos[0]):
                    try_breakup = True
                    self.player.state+= 1
            if(try_breakup):
                for g in self.girls:
                    if g.follow:
                        b = g.breakup(False)
                        if not b:
                            self.girls.append(Girl((self.player.pos[0]+get_screen_size()[0],-200), factor=self.factor))
                            self.player.lock = ((0,int(self.player.pos[0]+get_screen_size()[0]*1.25)))
                            del self.texts[:]
                            self.player.state = 0
                            self.player.show_heart = False
                        break
        player_pos = self.player.loop(screen, self.screen_pos,heart_factor=self.heart_factor)
           
        if(player_pos[0]>0):
            if((player_pos[0] < self.end or self.end == -1) and not self.end_reached):
                self.screen_pos = (player_pos[0],self.screen_pos[1])
        #Beginning the end
        if(player_pos[0]>self.end+get_screen_size()[0]/2 and self.end != -1 and not self.end_reached):
            self.end_reached = True
            self.count1 = 60
            self.count2 = 60
            end_text_pos = self.texts[len(self.texts)-1].pos
            self.texts.append(Text((end_text_pos[0],200),'is not the end',alpha=0, alpha_increase=True))
            self.player.lock = (self.end+get_screen_size()[0]/2+self.player.size[0],self.end+get_screen_size()[0]+self.player.size[0]/2)
            for g in self.girls:
                if g.follow:
                    g.alpha_decrease = True
                    break
        #The End
        if self.end_reached:
            if self.texts[len(self.texts)-1].alpha == 255:
                if self.count1 > 0:
                    self.count1 -= 1
                elif self.texts[len(self.texts)-1].text != 'R to retry':
                    self.texts[len(self.texts)-1].decrease_alpha = True
                    self.texts[len(self.texts)-2].decrease_alpha = True
            elif self.texts[len(self.texts)-1].alpha == 0 and self.count1 == 0 :
                end_text_pos = self.texts[len(self.texts)-1].pos
                if self.texts[len(self.texts)-1].text == 'is not the end':
                    self.texts.append(Text((end_text_pos[0],0), 'It is just',alpha=0,alpha_increase=True))
                    self.texts.append(Text(end_text_pos,'the beginning',alpha=0,alpha_increase=True))
                elif self.texts[len(self.texts)-1].text == 'the beginning':
                    self.texts.append(Text((end_text_pos[0],0), 'The One',alpha=0,alpha_increase=True))
                    self.texts.append(Text(end_text_pos,'a game by kwakwa',alpha=0,alpha_increase=True))
                elif self.texts[len(self.texts)-1].text == 'a game by kwakwa':
                    self.texts.append(Text((end_text_pos[0],0), 'Game designer',alpha=0,alpha_increase=True))
                    self.texts.append(Text(end_text_pos,'Elias Farhan',alpha=0,alpha_increase=True))
                elif self.texts[len(self.texts)-1].text == 'Elias Farhan':
                    self.texts.append(Text((end_text_pos[0],0), 'Music by',alpha=0,alpha_increase=True))
                    self.texts.append(Text(end_text_pos,'Tenchi',alpha=0,alpha_increase=True))
                elif self.texts[len(self.texts)-1].text == 'Tenchi':
                    self.texts.append(Text((end_text_pos[0],0), 'Duck sound by',alpha=0,alpha_increase=True))
                    self.texts.append(Text(end_text_pos,'Mike Koenig',alpha=0,alpha_increase=True))
                elif self.texts[len(self.texts)-1].text == 'Mike Koenig':
                    self.texts.append(Text((end_text_pos[0],0), 'ESC to quit',alpha=0,alpha_increase=True))
                    self.texts.append(Text(end_text_pos,'R to retry',alpha=0,alpha_increase=True))
                self.count1 = 60
        if get_retry():
            from engine.level_manager import switch
            switch('game')
        
            
        