from engine.scene import Scene
from engine.init import get_screen_size
from game_object.text import Text
import pygame
#font_obj, msg, sound_obj

class Kwakwa(Scene):
	def init(self):
		self.text = Text((get_screen_size()[0]/2, get_screen_size()[1]/2), 'Kwakwa', alpha=0, alpha_increase=True)
		self.count = 60
		pygame.mixer.music.load("data/sound/pissed_off_duck.wav")
		pygame.mixer.music.play()
	def loop(self, screen):
		screen.fill(pygame.Color(255, 255, 255))
		self.text.loop(screen, (0,0))
		if self.text.alpha == 255:
			if self.count == 0:
				self.text.decrease_alpha = True
			else:
				self.count -=1
		if self.text.alpha == 0 and self.count == 0:
			import engine.level_manager as level_manager
			level_manager.switch("game")