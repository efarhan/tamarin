from levels.logo_kwakwa import Kwakwa as logo_kwakwa
from levels.game import Game as game

dict_level = { "logo_kwakwa" : logo_kwakwa, "game" : game, }

level = 0
def switch(level_name):
	global level
	print 'switch to '+level_name
	try:
		level = dict_level[level_name]()
	except KeyError:
		level = 0
	if level != 0:
		level.init()
def function_level():
	if level == 0:
		return level
	return level.loop
