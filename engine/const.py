'''
Created on 8 sept. 2013

@author: efarhan
'''
import sys
from engine.init import get_screen_size
#constant for physics and gameplay
framerate = 60
g_dist = 100
speed = 4.0/1900
startup = "logo_kwakwa"

refusal = [ "I devout myself to my career", "How about no","Just No", "You are not my type", "I have no time for this now", "You are just a friend","No","We have nothing in common"]
approval = [ "Why not", "Yes Sure","Yes of course","Yes"]
after_break = ["Asshole", "You should not have broken up with me", "I loved you","Maybe next time"]
break_up = ["I do not love you", "You were the chosen one", "Sorry but I cannot go on", "I just wanted to have sex", \
            "I do not feel this thing with you", "Thanks for the money", "It is hard to say but it is over", "I am not happy"]