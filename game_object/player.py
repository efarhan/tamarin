'''
Created on 8 sept. 2013

@author: efarhan
'''
import pygame
from game_object import GameObject
from engine.event import get_keys
from engine.const import speed
from engine.init import get_screen_size

class Player(GameObject):
    def __init__(self,pos,move=0,jump=1,factor=1):
        GameObject.__init__(self)
        self.size = (32,32)
        self.pos = pos
        self.speed = int(speed*get_screen_size()[0])
        if(factor != 1):
            self.size = (factor*self.size[0],factor*self.size[1])
        
        path = 'data/sprites/boy/boy1.png'
        self.img = (self.img_manager.load_with_size(path, self.size))

        
        self.show_heart = False
        self.UP, self.RIGHT,self.LEFT,self.DOWN,self.ACTION = 0, 0, 0, 0, 0
        self.font = pygame.font.Font('data/font/8-BITWONDER.ttf',25)
        self.lock = None
        self.state = 0
    def loop(self, screen,screen_pos,new_size=1,heart_factor=1):
        #check event
        (self.RIGHT, self.LEFT,self.UP,self.DOWN,self.ACTION) = get_keys()       
        # set animation and velocity
        if((self.pos[0] > -get_screen_size()[0]/2+45 or (self.RIGHT and not self.LEFT)) and \
           (self.lock == None or ((self.pos[0]>self.lock[0] or (self.RIGHT and not self.LEFT)) and\
                                   ((self.pos[0]<self.lock[1]) or (not self.RIGHT and self.LEFT))))):
            self.pos = (self.pos[0]+self.speed*(self.RIGHT-self.LEFT),self.pos[1])
        # show the current img
        
        self.img_manager.show(self.img, screen, (self.pos[0]-screen_pos[0],self.pos[1]-screen_pos[1]),factor=new_size)
        return self.pos

        