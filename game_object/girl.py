'''
Created on 23 sept. 2013

@author: efarhan
'''

import pygame
import names
import math
import random
import engine
from engine.const import refusal, approval, after_break, break_up
from game_object import GameObject
from text import Text
from engine.const import g_dist
from engine.init import get_screen_size

class Girl(GameObject):
    def __init__(self,pos,factor=1):
        GameObject.__init__(self)
        self.size = (32,32)
        self.pos = pos
        if(factor != 1):
            self.size = (factor*self.size[0],factor*self.size[1])
        
        path = 'data/sprites/girl/girl1.png'
        arrow = 'data/sprites/up.png'
        self.img = (self.img_manager.load_with_size(path, self.size))
        self.arrow = (self.img_manager.load(arrow))
        self.font = pygame.font.Font('data/font/8-BITWONDER.ttf',25)
        self.name = names.get_first_name(gender='female')
        self.probability = random.random()
        self.prob_break = math.fabs(self.probability-0.5)
        self.follow = False
        self.flip = False
        self.message = ''
        self.indication = ['try relation', 'break up']
        self.texts = [Text((get_screen_size()[0]/2+self.pos[0],get_screen_size()[1]/2-self.pos[1]), self.message, 25),\
                      Text((get_screen_size()[0]/2+self.pos[0], get_screen_size()[1]/2-self.pos[1]-self.size[1]*3/2),self.indication[self.follow],25),\
                      Text((get_screen_size()[0]/2+self.pos[0], get_screen_size()[1]/2-self.pos[1]-self.size[1]), self.name, 25)]
        self.alpha_decrease = False
    def loop(self, screen,screen_pos,new_size=1):
        if(self.alpha_decrease):
            for t in self.texts:
                t.decrease_alpha = True
        if(self.follow):
            player_pos = engine.level_manager.level.player.pos
            if(math.fabs(player_pos[0]-self.pos[0])>g_dist):
                direction = player_pos[0]-self.pos[0]
                direction /= math.fabs(direction)
                self.flip = direction > 0
                self.pos = (player_pos[0]-direction*g_dist,self.pos[1]) 
            
        if self.message != '':
            self.texts[0].set_text(self.message)
            self.texts[0].pos = (screen.get_size()[0]/2+self.pos[0], screen.get_size()[1]/2-self.pos[1]-self.size[1]*3/2)
            self.texts[0].loop(screen,screen_pos)
        self.img_manager.show(self.img, screen, (self.pos[0]-screen_pos[0],self.pos[1]-screen_pos[1]),factor=new_size, flip_x=self.flip)
        if not self.alpha_decrease:
            self.img_manager.show(self.arrow,screen,(self.pos[0]-screen_pos[0]-80-self.size[0]/2,self.pos[1]-screen_pos[1]-self.size[1]-16),flip_y=self.follow)
        
        self.texts[1].set_text(self.indication[self.follow])
        self.texts[1].pos = (screen.get_size()[0]/2+self.pos[0], screen.get_size()[1]/2-self.pos[1]+self.size[1])
        self.texts[1].loop(screen,screen_pos)
        
        self.texts[2].pos = (screen.get_size()[0]/2+self.pos[0], screen.get_size()[1]/2-self.pos[1]-self.size[1])
        self.texts[2].loop(screen, screen_pos)
    def try_relation(self):
        success = random.random()<self.probability
        if success:
            self.message = random.choice(approval)
        else:
            self.message = random.choice(refusal)
        self.probability /= 4.0
        if success:
            self.probability = 0
            self.prob_break = math.fabs(self.probability-0.5)
        self.follow = success
        return success
    def breakup(self,you = True):
        #change message to break up
        if you:
            self.message = random.choice(after_break)
            self.follow = False
        else:
            if(random.random()<self.prob_break):
                self.message = random.choice(break_up)
                self.follow = False
            else:
                self.prob_break /= 4.0
                
        return self.follow
                
            